# New Benchmarks for Vehicle Routing with Time-Dependent Travel Times

This repository contains the new vehicle routing benchmark instances that
we created as part of the paper
J. Blauth, S. Held, D. Müller, N. Schlomberg, V. Traub, T. Tröbst, and J. Vygen: 
Vehicle Routing with Time-Dependent Travel Times: Theory, Practice, and Benchmarks. 
[Discrete Optimization, Volume 53,100848, 2024](https://www.sciencedirect.com/science/article/pii/S1572528624000276),
 see also [https://arxiv.org/abs/2205.00889](https://arxiv.org/abs/2205.00889). To create this benchmark set, we used map data
copyrighted by OpenStreetMap contributors and available from
<https://www.openstreetmap.org> under the [Open Database License (ODbL) v1.0](https://opendatacommons.org/licenses/odbl/1-0), 
and speed data retrieved from Uber Movement, (c) 2022 Uber Technologies, Inc., <https://movement.uber.com>, 
under a [Creative Commons Attribution Non-Commercial license](https://creativecommons.org/licenses/by-nc/3.0/us).
The benchmarks as they were used in the paper are also archived as [Blauth, Jannis; Held, Stephan; Müller, Dirk; Schlomberg, Niklas; Traub, Vera; Tröbst, Thorben; Vygen, Jens, 2024, "BonnTour: benchmarks & solutions for vehicle routing with time-dependent travel times", https://doi.org/10.60507/FK2/X22BKR, bonndata, V1](https://bonndata.uni-bonn.de/dataset.xhtml?persistentId=doi:10.60507/FK2/X22BKR).

We chose 10 cities from four sub-continents, for which we found a sufficient coverage of
streets with Uber speed data. For each of the cities, we chose a depot address, in most cases
close to a major airport, and then defined four delivery-only problem instances with 2000,
1000, 500, and 10 delivery addresses, respectively (the smallest one is provided mainly for
testing purposes). From each of these instances, in addition we derive a mixed pickup-and-
delivery instance as described below.

In the delivery-only instances, for each delivery address a, we defined one item to be picked
up at the depot without time restrictions and delivered at a within an individual time window.
Each tour must start at the depot no earlier than 15:00, and end at the depot at any time,
and can pick up items only at tour start. Pick-up takes zero time (i.e., we assume pre-loaded
vehicles). Each delivery of an item takes 3 minutes and has to begin within the delivery time
window, but may extend past its end. Time windows are chosen independently at random
for each address. With 50 % probability we chose a one-hour time window beginning at a full
or half hour chosen uniformly at random from {15:30, 16:00, . . . , 19:30, 20:00}; the remaining
items have a large delivery time window from 15:30 to 21:00. There is only one type of vehicles
with unlimited availability and unlimited capacity. Each vehicle has a fixed cost of $200. The
objective is to minimize total fixed cost plus the working time cost, which we set to $20 per
hour. 

Our mixed pickup-delivery problems are derived from the delivery-only instances as follows. Each of them uses the same addresses, and the same time window for each address 
as in the corresponding delivery-only instance. To simulate a realistic scenario, we sampled a set $`S \subseteq A`$ of *shops* from the addresses $`A`$ in an instance, with $`|S| = 0.2|A|`$ 
and such that each shop is at an address with a large time window. We then randomly chose a set of $`|A|`$ items $`(s, c, t) \in S \times (A \setminus S) \times \{delivery, return\}`$ with 

- $`t = delivery`$ with 80 % probability (denoting that the item is to be picked up at $`s`$ and delivered at $`c`$ (a "customer")),
- $`t = return`$ with 20 % probability (denoting that the item is to be picked up at $`c`$ and delivered at $`s`$), and
- a flightline distance $`d(s,c)`$ between $`s`$ and $`c`$ of at least 1 km, 
- the probability of $`(s,c,t)`$ being chosen exponentially decreasing with $`d(s,c)`$, 
- such that a delivery is possible with at least 15 minutes of slack.

Each visit at a shop or customer address takes three minutes, independent of the number of items picked up or delivered during that visit, and 
has to start within the time window of the address. 
Like in the delivery-only instances, each tour has to start at the depot no earlier than 15:00, end at the depot at any time, and the same objective costs are to be optimized. 
The depot address is not contained in the set $`A`$ of pickup and delivery addresses, 
and visiting the depot takes zero time. 

We provide approximately fastest travel times between any pair of addresses for the
relevant interval of departure times, which is [15:00, 21:03]. In order to allow for evaluation of
solutions with moderate time window violations, we also provide travel time data for departure
times up to at least half an hour past that interval. Although travelling distance is not part of
our objective function, our published benchmark data also contains the length of each fastest
path because it might be useful for future studies.
See [our paper](https://arxiv.org/abs/2205.00889) for further details, results we obtained
on our benchmark instances, and tour plots for a subset of them.

In the [instances](https://gitlab.com/muelleratorunibonnde/vrptdt-benchmark/-/tree/main/instances) folder, 
we provide our benchmark data in a JSON format that is described in detail in 
[vrp_tdt_benchmark.pdf](https://gitlab.com/muelleratorunibonnde/vrptdt-benchmark/-/blob/main/vrp_tdt_benchmark.pdf).
In addition to the benchmark data we provide a Python script `eval_tours.py`
that evaluates feasibility and cost of tour plans given in a simple data
format that is also described in that document.

The solutions that we reported in the high effort column of Tables 2 and 3 in [our paper](https://arxiv.org/abs/2205.00889)
are provided in the folder [solutions](https://gitlab.com/muelleratorunibonnde/vrptdt-benchmark/-/tree/main/solutions).

## License

![Creative Commons Attribution-NonCommercial 4.0 International License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-NonCommercial
4.0 International License](https://creativecommons.org/licenses/by-nc/4.0).
