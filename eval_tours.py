#!/usr/bin/env python3

import argparse
import bz2
import json
import sys
import math
from collections import defaultdict
from typing import List
from fractions import Fraction
from numbers import Rational

def visit_time(address):  # 0 for address == "depot", 3 minutes (in milliseconds) for all other addresses
    return 0 if address == "depot" else 3 * 60 * 1000

def cost_per_tour():
    return 200

def cost_per_hour():
    return 20

def one_minute():
    return 60 * 1000
    
def worktime_begin():
    return 15 * 60 * 60 * 1000

# Report number of items whose delivery slack in minutes is within half-open intervals [0, 5), [5, 10), and [10, 15), respectively
def slack_reporting_buckets():
    return [15, 10, 5]

def assert_list_of_ints(l):
    for x in l:
        if not isinstance(x, int):
            raise RuntimeError("Expected list of integers")

def create_matrix_index(tdt_matrix):
    matrix_index = {}
    for idx in range(len(tdt_matrix)):
        from_addr = tdt_matrix[idx]["from"]
        if not from_addr in matrix_index:
            matrix_index[from_addr] = {}
        matrix_index[from_addr][tdt_matrix[idx]["to"]] = idx
    return matrix_index

class ATF:
    def __init__(self, leave_times: List[int], arrival_times: List[int], dist_end_times: List[int], dists: List[int]):
        assert_list_of_ints(leave_times)
        assert_list_of_ints(arrival_times)
        assert_list_of_ints(dist_end_times)
        assert_list_of_ints(dists)
        self.leave_times = leave_times
        self.arrival_times = arrival_times
        self.dist_end_times = dist_end_times
        self.dists = dists
        self.size = len(leave_times)
        if self.size != len(arrival_times): 
            raise RuntimeError(f"ATF: {len(arrival_times)} arrival times, but {len(leave_times)} leave times")
        for idx in range(self.size):            
            if self.leave_times[idx] > self.arrival_times[idx]:
                raise RuntimeError(f"ATF: arrival time at index {idx} is less than leave time")
            if idx > 0:
                if self.leave_times[idx] <= self.leave_times[idx-1]:
                    raise RuntimeError(f"ATF: leave time at index {idx} is not strictly greater than leave time at index {idx - 1}")
                if self.arrival_times[idx] < self.arrival_times[idx-1]:
                    raise RuntimeError(f"ATF: arrival time at index {idx} is smaller than arrival time at index {idx - 1}")
        if len(self.dists) != len(self.dist_end_times):
            raise RuntimeError(f"ATF: {len(self.dists)} dists entries, but {len(self.dist_end_times)} dist_end_times entries")
        for idx, t in enumerate(self.dist_end_times):
            if not t in self.leave_times:
                raise RuntimeError(f"ATF: {t} in dist_end_times is not a leave time of a breakpoint")
            if idx > 0 and self.dist_end_times[idx - 1] >= self.dist_end_times[idx]:
                raise RuntimeError(f"ATF: dist_end_times[{idx}] is not strictly greater than dist_end_times[{idx-1}]")
        if self.size > 0 and (len(self.dist_end_times) == 0 or self.dist_end_times[-1] < self.leave_times[-1]):
            raise RuntimeError("ATF: travel distance data does not cover domain")
            
    # Raises an exception if arrival time function is empty or t is after the end of the domain.
    # For t earlier than self.leave_times[0] it is possible to wait until self.leave_times[0], so we 
    # don't report an error in that case but implicitly start the domain at minus infinity
    def domain_check(self, t: Rational):
        if self.size == 0:
            raise RuntimeError(f"ATF: empty, cannot evaluate at time {t}")
        if t > self.leave_times[-1]:
            raise RuntimeError(f"ATF: cannot evaluate at time {t} which is past the end of domain at {self.leave_times[-1]}")

    def eval(self, t: Rational):
        self.domain_check(t)
        if t <= self.leave_times[0]:
            return self.arrival_times[0]
        # By the above checks, we get last_idx_before != None and 0 <= last_idx_before < self.size - 1
        last_idx_before = next(idx for idx, val in enumerate(self.leave_times) if val >= t) - 1 
        x1 = self.leave_times[last_idx_before]
        y1 = self.arrival_times[last_idx_before]
        x2 = self.leave_times[last_idx_before + 1]
        y2 = self.arrival_times[last_idx_before + 1]
        return y1 + Fraction((t - x1) * (y2 - y1), x2 - x1)


    def eval_dist(self, t: Rational):
        self.domain_check(t)
        if t <= self.leave_times[0]:
            return self.dists[0]
        # By the above checks, there is an entry in self.dist_end_times greater than t 
        first_not_smaller = next(idx for idx, val in enumerate(self.dist_end_times) if val >= t)
        return self.dists[first_not_smaller]

        
def read_json(filename):
    if filename.endswith('.json'):
        open_cmd = open
    elif filename.endswith('.json.bz2'):
        open_cmd = bz2.open
    else:
        raise ValueError(f'Unknown json extension {filename}')
    with open_cmd(filename) as f:
        return json.load(f)

def write_json(data, filename):
    if filename:
        with open(filename, 'w') as f:
           json.dump(data, f, indent=3)
    else:
        json.dump(data, sys.stdout, indent=3)

class EvalSolutions:
    def __init__(self, instance, tdt_matrix, args):
        self.instance = instance
        self.tdt_matrix = tdt_matrix
        self.args = args
        self.matrix_index = create_matrix_index(self.tdt_matrix)

    def address(self, item, pickup : bool):
        if self.args.pickup_delivery:
            return self.instance["items"][item]["pickup_address"] if pickup else self.instance["items"][item]["delivery_address"]
        else:
            # item ID's are synonymous with delivery addresses
            return "depot" if pickup else item

    def time_window(self, item, pickup : bool):
        item = self.instance["items"][item]
        if self.args.pickup_delivery:
            return (item["earliest_pickup"], item["latest_pickup"]) if pickup else (item["earliest_delivery"], item["latest_delivery"])
        else:
            # for delivery-only instances, make the time windows for the artificial pick-ups sufficiently large (ending no earlier than the latest 
            # end of a delivery time window)
            return (worktime_begin(), 24 * 60 * 60 * 1000) if pickup else (item["earliest_delivery"], item["latest_delivery"])


    def run(self):
        result = {}
        for sol_filename in self.args.solutions:
            result[sol_filename] = self.eval_solution(sol_filename)
        if len(result) == 1:
            # If only one solution is to be evaluated, we put the result directly in the top level
            result = next(iter(result.values()))
        return result

    def eval_solution(self, sol_filename):
        sol = read_json(sol_filename)
        tour_results = []
        picked_up_items = set()
        delivered_items = set()
        tour_ids = []
        for tour in sol["tours"]:
            tour_results.append(self.eval_tour(tour, picked_up_items, delivered_items))
            tour_id = tour_results[-1]["tour_id"]
            if tour_id in tour_ids:
               raise RuntimeError(f"tour_id '{tour_id}' appears twice.")
            tour_ids.append(tour_id)
        result = {"aggregated" : self.aggregate_tour_results(tour_results), "dropped_items": [], "tour_results": tour_results}
        for item in sorted(self.instance["items"].keys()):
            if item not in delivered_items:
                if self.args.pickup_delivery:
                    result["aggregated"]["pickup_time_window_violations"][item] = math.inf
                    result["aggregated"]["delivery_time_window_violations"][item] = math.inf
                else:
                    result["aggregated"]["time_window_violations"][item] = math.inf
                result["dropped_items"].append(item)
                print(f"Solution {sol_filename}: item {item} is not delivered.", file=sys.stderr)
        return result

    def eval_tour(self, tour, picked_up_items, delivered_items):
        start_time = tour["start_time"]
        if not isinstance(start_time, int):
            raise RuntimeError(f"Tour {tour['tour_id']}: start_time is not an integer")
        if start_time < worktime_begin(): 
            raise RuntimeError(f"Tour {tour['tour_id']}: start_time is earlier than begin of worktime")

        if not self.args.pickup_delivery:
            # Convert to pickup-delivery format to use common evaluation code
            if "deliveries" not in tour.keys():
                raise RuntimeError(f"Tour {tour['tour_id']}: missing key 'deliveries'")
            actions = []
            delivery_actions = []
            for delivery in tour["deliveries"]:
                actions.append({"action": "pickup", "item": delivery})
                delivery_actions.append({"action": "delivery", "item": delivery})
            actions.extend(delivery_actions)
            tour["actions"] = actions 
            tour.pop("deliveries")

        current_addr = "depot"
        current_time = start_time
        driving_time = 0
        waiting_time = 0
        pickup_tw_violations = {}
        delivery_tw_violations = {}
        max_tw_viol = 0
        picked_up_in_this_tour = set()
        dist = 0
        slack_histogram_buckets = sorted(slack_reporting_buckets() + [0])  # add a bucket for negative slack, i.e. time window violations
        slack_histogram = {bucket: 0 for bucket in slack_histogram_buckets}
        for action in tour["actions"]:
            action_type = action["action"]
            item = action["item"]
            if not(action_type == "pickup" or action_type == "delivery"):
                raise RuntimeError(f"Tour {tour['tour_id']}: invalid action \"{action_type}\"")
            is_pickup = (action_type == "pickup")
            if self.args.pickup_delivery and is_pickup:
                if item in picked_up_items:
                    raise RuntimeError(f"Item {item} is picked up twice.")
                picked_up_items.add(item)
                picked_up_in_this_tour.add(item)
            if not is_pickup:
                if item in delivered_items:
                    raise RuntimeError(f"Item {item} is delivered twice.")
                delivered_items.add(item)
                if self.args.pickup_delivery and item not in picked_up_in_this_tour:
                    raise RuntimeError(f"Item {item} is delivered in tour {tour['tour_id']} without having been picked up before")
            
            addr = self.address(item, is_pickup)
            if addr != current_addr:
                current_time += visit_time(current_addr)
            atf_data = self.tdt_matrix[self.matrix_index[current_addr][addr]]["atf"] 
            atf = ATF(atf_data["atf_leave_time"], atf_data["atf_arrive_time"], atf_data["atf_dist_end_time"], atf_data["atf_distance"])
            arrival_time = atf.eval(current_time)
            dist += atf.eval_dist(current_time)
            driving_time += (arrival_time - current_time)
            tw_begin, tw_end = self.time_window(item, is_pickup)
            if not (isinstance(tw_begin, int) and isinstance(tw_end, int)):
               raise RuntimeError(f"Item {self.instance['items'][item]}: time window boundaries are not integral")
            slack = tw_end - arrival_time
            current_time = max(arrival_time, tw_begin)
            waiting_time += (current_time - arrival_time)
            if slack < 0:
                if is_pickup:
                    pickup_tw_violations[item] = math.ceil(-slack)
                else:
                    delivery_tw_violations[item] = math.ceil(-slack)
                max_tw_viol = max(max_tw_viol, math.ceil(-slack))
            slack_bucket = next((b for b in slack_histogram_buckets if b * one_minute() > slack), None)
            if slack_bucket != None and (self.args.pickup_delivery or not is_pickup): 
                # count pickup slacks only for pickup-delivery problem instances
                slack_histogram[slack_bucket] += 1

            current_addr = addr
        if len(picked_up_in_this_tour) * 2 > len(tour["actions"]):
            raise RuntimeError(f"There are more pick-ups than deliveries in tour {tour['tour_id']}")
            

        # drive back to depot
        if current_addr != "depot":
            current_time += visit_time(current_addr)
        atf_data = self.tdt_matrix[self.matrix_index[current_addr]["depot"]]["atf"] 
        atf = ATF(atf_data["atf_leave_time"], atf_data["atf_arrive_time"], atf_data["atf_dist_end_time"], atf_data["atf_distance"])
        dist += atf.eval_dist(current_time)
        arrival_time = atf.eval(current_time)
        driving_time += (arrival_time - current_time)
        current_time = arrival_time
        working_time = current_time - start_time

        result = {   "tour_id": tour["tour_id"], 
                     "working_time": math.ceil(working_time),
                     "driving_time": math.ceil(driving_time),
                     "waiting_time": math.ceil(waiting_time),
                     "distance": dist,
                     "max_time_window_violation": max_tw_viol,
                     "slack_histogram": slack_histogram
                 }

        if self.args.pickup_delivery:
            result["pickup_time_window_violations"] = pickup_tw_violations
            result["delivery_time_window_violations"] = delivery_tw_violations
        else:
            result["time_window_violations"] = delivery_tw_violations
        return result

    def aggregate_tour_results(self, tour_results):
        working_time = 0
        driving_time = 0
        waiting_time = 0
        pickup_tw_violations = {}
        delivery_tw_violations = {}
        max_tw_viol = 0
        dist = 0
        slack_histogram = defaultdict(int)
        for tour_result in tour_results:
            working_time += tour_result["working_time"]
            driving_time += tour_result["driving_time"]
            waiting_time += tour_result["waiting_time"]
            if self.args.pickup_delivery:
                pickup_tw_violations.update(tour_result["pickup_time_window_violations"])
                delivery_tw_violations.update(tour_result["delivery_time_window_violations"])
            else:
                delivery_tw_violations.update(tour_result["time_window_violations"])
            max_tw_viol = max(max_tw_viol, tour_result["max_time_window_violation"])
            dist += tour_result["distance"]
            for key, value in tour_result["slack_histogram"].items():
                slack_histogram[key] += value

        result = {   "num_tours": len(tour_results),
                     "working_time": working_time, 
                     "driving_time": driving_time,
                     "waiting_time": waiting_time,
                     "distance": dist,
                     "cost": cost_per_tour() * len(tour_results) + cost_per_hour() * (1.0 / 3600000) * working_time,
                     "max_time_window_violation": max_tw_viol,
                     "slack_histogram": slack_histogram
                 }
        if self.args.pickup_delivery:
            result["pickup_time_window_violations"] = pickup_tw_violations
            result["delivery_time_window_violations"] = delivery_tw_violations
        else:
            result["time_window_violations"] = delivery_tw_violations
        return result


def run(args):
    tdt_matrix = read_json(args.tdt_matrix)
    instance = read_json(args.items)
    eval_solutions = EvalSolutions(instance, tdt_matrix, args)
    result = eval_solutions.run()
    write_json(result, args.output)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--tdt-matrix", "-m", required=True, help="Time-dependent travel times")
    parser.add_argument("--items", "-i", required=True, help="Item and address data (using only addresses covered in travel time matrix)")
    parser.add_argument("--solutions", "-s", required=True, nargs="+", help="Solutions to be evaluated, each a JSON file") 
    parser.add_argument("--output", "-o", default=None, help="Output file name; prints to standard output if omitted")
    parser.add_argument("--pickup-delivery", "-pdp", action='store_true', help="Assume pickup-delivery problem format for items and solutions")
    return parser.parse_args()

def _main():
    args = get_args()
    run(args)

if __name__ == '__main__':
    _main()
