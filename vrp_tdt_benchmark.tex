\documentclass[11pt]{article}
\newcommand{\todo}{\textcolor{red}{TODO: }\textcolor{red}}
\usepackage{color}
\usepackage{amssymb}
\usepackage[margin=3cm]{geometry}
\usepackage[pdfpagelabels,bookmarks=false]{hyperref}
\usepackage{pgfplots}
\usepackage{subcaption}

\def\REV#1{\textcolor{black}{#1}}
\def\REVCOLOR{\color{black}}
\begin{document}

\makeatletter
\let\@fnsymbol\@arabic
\makeatother

\title{New Benchmarks for Vehicle Routing \\[1 mm] with Time-Dependent Travel Times}
\author{Jannis Blauth\thanks{Research Institute for Discrete Mathematics and Hausdorff Center for Mathematics, University of Bonn.}, Stephan Held\footnotemark[1], Dirk M\"uller\footnotemark[1], Niklas Schlomberg\footnotemark[1],\\[1mm]
	Vera Traub\footnotemark[1]~\thanks{Supported by the Swiss National Science Foundation grant 200021\_184622.}, Thorben Tr\"obst\thanks{University of California, Irvine.}, Jens Vygen\footnotemark[1]}

\date{}


%\tableofcontents
%\newpage

\maketitle

This document decribes the new vehicle routing benchmark instances that we created as part of our work on vehicle routing with time-dependent travel times \cite{vrptdt2022}.
To create this benchmark set, we used map data copyrighted by OpenStreetMap contributors and available from \url{https://www.openstreetmap.org} under the Open Database License (ODbL) v1.0
(see \url{https://opendatacommons.org/licenses/odbl/1-0}), and speed data retrieved from Uber Movement, (c) 2022 Uber Technologies, Inc., \url{https://movement.uber.com},
under a Creative Commons Attribution Non-Commercial license (see \url{https://creativecommons.org/licenses/by-nc/3.0/us}).

We chose 10 cities from four sub-continents, for which we found a sufficient coverage of
streets with Uber speed data. For each of the cities, we chose a depot address, in most cases close to a major airport, and
then defined \REV{four delivery-only} problem instances with 2000, 1000, 500, \REV{and 10} delivery addresses, respectively (the smallest one is provided mainly for testing purposes).
\REV{From each of these instances, in addition we derive a mixed pickup-and-delivery instance as described below.}

\REV{In the delivery-only instances, for} each delivery address $a$,
we defined one item to be picked up at the depot without time restrictions
and delivered at $a$ within an individual time window.
Each tour must start \REV{at the depot no earlier than 15:00}, and end at the depot \REV{at any time}, and can pick up items only at tour start.
Pick-up takes zero time (i.e., we assume pre-loaded vehicles). Each delivery of an item takes 3 minutes and has to begin within the delivery time window, but may extend past its end.
\REV{Time windows are chosen independently at random for each address. With 50\,\% probability we chose a one-hour time window beginning at a full or half hour chosen uniformly at random from
	$\{\textrm{15:30},\textrm{16:00},\dots,\textrm{19:30},\textrm{20:00}\}$; the remaining items have a large delivery time window from 15:30 to 21:00.}
There is only one type of vehicles with unlimited availability and unlimited capacity.
Each vehicle has a fixed cost of \$200. The objective is to minimize total fixed cost plus the working time cost, which we set to \$20 per hour.
We provide approximately fastest travel times between any pair of addresses for \REV{the relevant interval of departure times, which is [15:00, 21:03].
	In order to allow for evaluation of solutions with moderate time window violations, we also provide travel time data for departure times up to at least half an hour past that interval}.
Although travelling distance is not part of our objective function,
our published benchmark data also contains the length of each fastest path because it might be useful for future studies.

\REV{Our mixed pickup-and-delivery problems are derived from the delivery-only instances as follows. Each of them uses the same addresses, and the same time window for each address
	as in the corresponding delivery-only instance. To simulate a realistic scenario, we sampled a set $S \subseteq A$ of \emph{shops} from the addresses $A$ in an instance, with $|S| = 0.2|A|$
	and such that each shop is at an address with a large time window. We then randomly chose a set of $|A|$ items $(s, c, t) \in S \times (A \setminus S) \times \{delivery, return\}$ with
	\begin{itemize}
		\item $t = delivery$ with 80 \% probability (denoting that the item is to be picked up at $s$ and delivered at $c$ (a "customer")),
		\item $t = return$ with 20 \% probability (denoting that the item is to be picked up at $c$ and delivered at $s$), and
		\item a flightline distance $d(s,c)$ between $s$ and $c$ of at least 1 km,
		\item the probability of $(s,c,t)$ being chosen exponentially decreasing with $d(s,c)$,
		\item such that a delivery is possible with at least 15 minutes of slack.
	\end{itemize}
	Each visit at a shop or customer address takes three minutes, independent of the number of items picked up or delivered during that visit, and
	has to start within the time window of the address.
	Like in the delivery-only instances, each tour has to start at the depot no earlier than 15:00, end at the depot at any time, and the same objective costs are to be optimized.
	The depot address is not contained in the set $A$ of pickup and delivery addresses,
	and visiting the depot takes zero time.
}

See \cite{vrptdt2022} for further details and results we obtained on our benchmark instances. Tour plots can be found in Figure \ref{fig:tours} at the end of this document.
We provide our benchmark data in a JSON format that is described in Sections \ref{sec:item-data} \REV{(delivery-only instances), \ref{sec:item-data-pdp} (pickup-and-delivery instances),} and \ref{sec:tt-data} \REV{(travel times)}.
In addition to the benchmark data we provide a Python script (cf.~Section \ref{sec:eval-tours}) that evaluates feasibility and cost of tour plans given in a simple data format
described in Section\REV{s} \ref{sec:tour-format} \REV{and \ref{sec:tour-format-pdp}}.

This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License, see \url{https://creativecommons.org/licenses/by-nc/4.0}.

\section{Data format}
\label{sec:data-format}

Both input and tour data use the JSON data format. All time values are integers that denote milliseconds since midnight (e.g., 4pm would be specified by the value 57600000).
We write \texttt{a[n]} to denote the object stored at index $n \in \mathbb{N}_0$ in a JSON array \texttt{a}, and \texttt{d["key"]} for the object stored under \texttt{"key"} in a JSON dictionary \texttt{d}.

For each $\texttt{city} \in \left\{\texttt{berlin}, \texttt{cincinnati}, \texttt{kyiv}, \texttt{london}, \texttt{madrid}, \texttt{nairobi}, \texttt{new\_york}, \right.$\\
$\left. \texttt{san\_francisco}, \texttt{sao\_paulo}, \texttt{seattle}\right\}$
and each $\texttt{size} \in \{10, 500, 1000, 2000\}$ we provide \REV{three} files
\begin{itemize}
	\item \texttt{city\_size\_tt.json.bz2} specifying travel times and distances as \texttt{bzip2}-compressed JSON (cf.~Section \ref{sec:tt-data})
	\item \texttt{city\_size.json} specifying item and address data \REV{for the delivery-only instance in \texttt{city} with \texttt{size} many delivery tasks} (cf.~Section \ref{sec:item-data})
	      that together with the file \linebreak
	      \texttt{city\_size\_tt.json.bz2} defines the problem instance \texttt{city\_size}
	\item \REV{\texttt{city\_size\_pdp.json} specifying item and address data for the pickup-and-delivery instance in \texttt{city} with \texttt{size} many pickup-and-delivery tasks (cf.~Section \ref{sec:item-data-pdp})
		      that together with the file \texttt{city\_size\_tt.json.bz2} defines the problem instance \texttt{city\_size\_pdp}}
\end{itemize}
E.g., the problem instance \texttt{new\_york\_1000} is given by the files \texttt{new\_york\_1000.json} and  \texttt{new\_york\_1000\_tt.json.bz2}.

\subsection{Item and address data \REV{for delivery-only instances}}
\label{sec:item-data}
Item and address data for a \REV{delivery-only} problem instance is provided in a JSON file that contains a top-level key \texttt{items}, mapping
each item identifier to a JSON dictionary with keys
\begin{itemize}
	\item \texttt{earliest\_delivery}: start of the delivery time window
	\item \texttt{latest\_delivery}: end of the delivery time window
	\item \texttt{latitude}: The geographical latitude of the delivery address
	\item \texttt{longitude}: The geographical longitude of the delivery address
\end{itemize}
In addition, the top-level key \texttt{depot} maps to a dictionary with keys \texttt{latitude} and \texttt{longitude}
that specify the geographical coordinates of the depot address. While geographical coordinates are not required for
computing or evaluating solutions, we provide them for visualization purposes.

	{\bf Example.} The following data defines a depot address and two items \texttt{"1"} and \texttt{"2"} with their address data and time windows in Berlin:

\begin{verbatim}
{
    "depot": {
        "latitude": 52.483484543993242,
        "longitude": 13.387688274540734
    },
    "items": {
        "1": {
            "earliest_delivery": 55800000,
            "latest_delivery": 75600000,
            "latitude": 52.56101660004704,
            "longitude": 13.365484533476485
        },
        "2": {
            "earliest_delivery": 64800000,
            "latest_delivery": 68400000,
            "latitude": 52.50504420416935,
            "longitude": 13.337653605240904
        }
    }
}
\end{verbatim}

{\REVCOLOR
\subsection{\REV{Item and address data for pickup-and-delivery instances}}
\label{sec:item-data-pdp}

Item and address data for a \REV{pickup-and-delivery} problem instance is split into two JSON dictionaries under top-level keys \texttt{addresses} and \texttt{items}.

The \texttt{addresses} dictionary maps each address identifier to the geographical coordinates of the address, stored in a dictionary with keys
\begin{itemize}
	\item \texttt{latitude}: The geographical latitude of the address
	\item \texttt{longitude}: The geographical longitude of the address
\end{itemize}
The \texttt{items} dictionary maps each item identifier to a JSON dictionary with keys
\begin{itemize}
	\item \texttt{pickup\_address}: The identifier of the pickup address
	\item \texttt{delivery\_address}: The identifier of the delivery address
	\item \texttt{earliest\_pickup}: start of the pickup time window
	\item \texttt{latest\_pickup}: end of the pickup time window
	\item \texttt{earliest\_delivery}: start of the delivery time window
	\item \texttt{latest\_delivery}: end of the delivery time window
\end{itemize}
In addition, the top-level key \texttt{depot} maps to a dictionary with keys \texttt{latitude} and \texttt{longitude}
that specify the geographical coordinates of the depot address.

	{\bf Example.} The following data defines a depot address and three pickup/delivery addresses, and two items \texttt{"1"} and \texttt{"2"} with their pickup and delivery addresses and time windows in Berlin:
{ \small
\begin{verbatim}
{
  "depot": {
    "latitude": 52.48348454399324,
    "longitude": 13.387688274540734
  },
  "addresses": {
    "1": {
      "latitude": 52.50504420416935,
      "longitude": 13.337653605240904
    },
    "2": {
      "latitude": 52.47528740274032,
      "longitude": 13.439771512964708
    },
    "3": {
      "latitude": 52.49076663639406,
      "longitude": 13.456408013541068
    }
  },
\end{verbatim}
\pagebreak
\begin{verbatim}
  "items": {
    "1": {
      "pickup_address": "2",
      "delivery_address": "1",
      "earliest_pickup": 55800000,
      "latest_pickup": 75600000,
      "earliest_delivery": 64800000,
      "latest_delivery": 68400000
    },
    "2": {
      "pickup_address": "3",
      "delivery_address": "2",
      "earliest_pickup": 55800000,
      "latest_pickup": 75600000,
      "earliest_delivery": 55800000,
      "latest_delivery": 75600000
    }
  }
}
\end{verbatim}
}

Note that address \texttt{"2"} is both a pickup address (for item \texttt{"1"}) and a delivery address (for item \texttt{"2"}).

Although in the construction of our instances we use the same time window for all pick-ups and deliveries at the same address, we specify time windows as part of the item data
instead of making them part of the address data.
This allows to define future problem instances with different time windows at the same address, without changing the data format.
}

\subsection{Travel time and distance data}
\label{sec:tt-data}

Travel time data is given in JSON format as an array of dictionaries, each with keys \texttt{from}, \texttt{to}, and \texttt{atf}.
For each $(a,b) \in (A \cup \{\texttt{"depot"}\}) \times (A \cup \{\texttt{"depot"}\})$, where $A$ is the set of item identifiers \REV{(for delivery-only instances) or address identifiers, respectively (for pickup-and-delivery instances),}
in the corresponding item and address data file, there is exactly one entry in this array,
with $a$ equal to the value stored under the \texttt{from} key, and $b$ the value stored under the \texttt{to} key. The \texttt{atf}
key of the entry stores a dictionary that encodes a piecewise linear arrival time function, mapping departure times at $a$ to approximately earliest possible arrival times at $b$, and the
corresponding travelling distances. The dictionary comprises the keys
\begin{itemize}
	\item \texttt{atf\_leave\_time}: An array of strictly monotonically increasing departure times of the supporting points of the piecewise linear arrival time function
	\item \texttt{atf\_arrive\_time}: An array of the same size, with \texttt{atf\_arrive\_time[i]} specifying the arrival time at $b$ when leaving $a$ at \texttt{atf\_leave\_time[i]}
	\item \texttt{atf\_dist\_end\_time}: A subsequence of \texttt{atf\_leave\_time}. I.e., each entry in \texttt{atf\_dist\_end\_time} also appears in \texttt{atf\_leave\_time},
	      and \texttt{atf\_dist\_end\_time} is strictly monotonically increasing
	\item \texttt{atf\_distance}: An array of travelling distance values in meters. The array has the same number $n$ of elements as \texttt{atf\_dist\_end\_time}, and
	      \texttt{atf\_distance[i]} ($0 \leq i < n$) specifies the travelling distance for departure times less or equal to \texttt{atf\_dist\_end\_time[i]}, and greater than \texttt{atf\_dist\_end\_time[i-1]} if $i>0$.
\end{itemize}

{\bf Example.} The following example shows one entry encoding travel time and distance data from \texttt{"depot"} to address \texttt{"7"}:
\begin{verbatim}
{
  "from": "depot",
  "to": "7",
  "atf": {
    "atf_leave_time": [ 54000000, 59400000, 63000000, 66600000, 
                        70200000, 73800000, 79200000 ],
    "atf_arrive_time": [ 55800000, 61740000, 66060000, 69840000, 
                         73080000, 75600000, 80460000 ], 
    "atf_dist_end_time": [ 63000000, 79200000 ],
    "atf_distance": [ 9500, 8000 ]
  }
}
\end{verbatim}

In this example, the fastest travel time is attained along a path $A$ of length 9.5 kilometers when departing from 3 pm to 5:30 pm, and during that time increases from 30 to 51 minutes.
After 5:30 pm, a different path $B$ of length 8 kilometers attains fastest travel times, which still increase slightly until 6:30 pm, and then start to decrease until 10 pm, when only 21 minutes are needed.
The following graph shows the time-dependent travel times in this example, colored blue for departure times at which path $A$ is fastest, and green when $B$ is fastest:

\begin{center}
	\begin{tikzpicture}
		\begin{axis}[
				xtick={54000, 61200, 68400, 75600, 82800},
				xticklabels={3pm, 5pm, 7pm, 9pm, 11pm},
				scaled x ticks=false,
				minor x tick num=1,
				xlabel={departure time},
				ylabel={travel time [minutes]}
			]
			\addplot[blue,domain=54000:59400] {(1800+(x-54000)*0.1) / 60.0};
			\addplot[blue,domain=59400:63000] {(1800+540+(x-59400)*0.2) / 60.0};
			\addplot[green,domain=63000:66600] {(1800+1260+(x-63000)*0.05) / 60.0};
			\addplot[green,domain=66600:70200] {(1800+1440-(x-66600)*0.1) / 60.0};
			\addplot[green,domain=70200:73800] {(1800+1080-(x-70200)*0.3) / 60.0};
			\addplot[green,domain=73800:79200] {(1800-(x-73800)*0.1) / 60.0};
		\end{axis}
	\end{tikzpicture}
\end{center}

\pagebreak
\subsection{Tour data \REV{format for delivery-only instances}}
\label{sec:tour-format}
Our tour evaluation script described in Section \ref{sec:eval-tours} \REV{can read} tour data from a JSON file \REV{in the following format for delivery-only instances: under a}
top-level key \texttt{tours} an array of tour objects is stored. \REV{Each of those is} a dictionary with keys
\begin{itemize}
	\item \texttt{tour\_id}: stores a string used as tour identifier
	\item \texttt{start\_time}: departure time of the tour at the depot
	\item \texttt{deliveries}: an array of item identifiers that are delivered by this tour in the given order
\end{itemize}
The \texttt{tour\_id}'s must be unique, and each item must be delivered exactly once.

	{\bf Example.} The following tour file defines a single tour with identifier \texttt{1}, starting at the depot at 4:30 pm, delivering items
\texttt{6}, \texttt{5}, \texttt{8}, \texttt{4}, \texttt{9}, \texttt{7}, \texttt{2}, \texttt{10}, \texttt{1}, and \texttt{3} in that order, and returning to the depot in the end:
\begin{verbatim}
{
    "tours": [
        {
            "tour_id": "1",
            "start_time": 59400000,
            "deliveries": [ "6", "5", "8", "4", "9", "7", "2", "10", "1", "3" ]
        }
    ]
}
\end{verbatim}

{\REVCOLOR
\subsection{Tour data format for pickup-and-delivery instances}
\label{sec:tour-format-pdp}
Our tour evaluation script described in Section \ref{sec:eval-tours} can also read tour data \REV{in the following alternative format for pickup-and-delivery instances: under a}
top-level key \texttt{tours} an array of tour objects is stored. Each of those is a dictionary with keys
\begin{itemize}
	\item \texttt{tour\_id}: stores a string used as tour identifier
	\item \texttt{start\_time}: departure time of the tour at the depot
	\item \texttt{actions}: an array of actions performed by this tour in the given order, each given by a dictionary with keys
	      \begin{itemize}
		      \item \texttt{action}: specifying the type of action, either \texttt{"pickup"} or \texttt{"delivery"}
		      \item \texttt{item}: the identifier of the item that is picked up or delivered in this action
	      \end{itemize}
\end{itemize}
The \texttt{tour\_id}'s must be unique, each item must be picked up exactly once, and must be delivered exactly once and in the same tour after it has been picked up.

	{\bf Example.} The following tour file defines a single tour with identifier \texttt{1}, starting at the depot at 3:57:50 pm, picking up item 3, picking up item 1, delivering item 1,
picking up item 2, delivering item 3, delivering item 2 (in that order), and returning to the depot in the end:

{\small
\begin{verbatim}
{
    "tours": [
        {
            "tour_id": "1",
            "start_time": 57470000,
            "actions": [
                { "action": "pickup", "item": "3" },
                { "action": "pickup", "item": "1" },
                { "action": "delivery", "item": "1" },
                { "action": "pickup", "item": "2" },
                { "action": "delivery", "item": "3" },
                { "action": "delivery", "item": "2" }
            ]
        }
    ]
}
\end{verbatim}
}
}

\section{Solution evaluation}
\label{sec:eval-tours}
We provide a Python script \texttt{eval\_tours.py} to evaluate tours given in the format described in Section \ref{sec:tour-format} \REV{for delivery-only instances, or  in the format described in Section \ref{sec:tour-format-pdp} for pickup-and-delivery instances}.
It takes three required arguments:

\begin{itemize}
	\item the item and address data file, specified with the option \texttt{-i}
	\item the file containing the travel time matrix, specified with the option \texttt{-m}
	\item a tour file containing a solution for the problem instance, specified with the option \texttt{-s}
\end{itemize}

\REV{In addition, an optional argument \texttt{-pdp} can be given to indicate that the instance and the tour data are given in the format for pickup-and-delivery problems. If this argument is omitted, instance and tour data are
	assumed to be in the format for delivery-only instances.}

For example,
\begin{verbatim}
eval_tours.py -i berlin_1000.json -m berlin_1000_tt.json.bz2 -s solution.json
\end{verbatim}
evaluates a solution for the \texttt{berlin\_1000} \REV{(delivery-only)} problem instance given in the file \texttt{solution.json}\REV{, while}
{\REVCOLOR\footnotesize
	\begin{verbatim}
eval_tours.py -i berlin_1000_pdp.json -m berlin_1000_tt.json.bz2 -s solution_pdp.json -pdp
\end{verbatim}\normalsize
	evaluates a solution for the \texttt{berlin\_1000\_pdp} \REV{(pickup-and-delivery)} problem instance given in the file \texttt{solution\_pdp.json}.
}

The evaluation script checks the conditions imposed on the instance and tour data in Section \ref{sec:data-format} and throws a \texttt{RuntimeError}
if they are not satisfied. It uses exact rational arithmetic to guarantee that time window violations are analyzed without loss of precision
that could occur with floating point arithmetic, and without conservatism by rounding up travel times between stops to integral values.

The results are written to the standard output by default, but can be sent to a file instead with the option \texttt{-o}.
Total working time, waiting time, driving time, cost and distance are reported under the top-level key \texttt{aggregated},
in the same units as used in the input, with time values rounded up to the next integer if they are not integral.
Waiting time occurs if the arrival time $t_a(i)$ at the delivery address of an item $i$ is less than
\texttt{items[i]["earliest\_delivery\_time"]}, where \texttt{items} refers to the array of items in the input file described in Section \ref{sec:item-data}.
In addition, time window violations are reported as a dictionary of item identifiers mapped
to numerical values denoting the amount of time in milliseconds that the corresponding delivery is too late in this solution, again rounded up to integral values (the analysis is exact
though, so a violation of one millisecond reported guarantees that there is a violation).
Finally, there is a slack histogram which is given as a dictionary with keys \texttt{0}, \texttt{5}, \texttt{10}, and \texttt{15}
that store the number of items \texttt{i} with $slack(\texttt{i}) := \texttt{items[i]["latest\_delivery\_time"]} - t_a(i)$ less than the corresponding
amount of minutes, but at least the next lower value if non-zero. In particular, under the key \texttt{0} the number of time window violations is reported, i.e., deliveries with negative slack.

Under the top-level key \texttt{tour\_results}, these statistics are also available for individual tours, identified by their \texttt{tour\_id}.

\REV{Under the top-level key \texttt{dropped\_items}, items are listed that are not delivered by any tour. These items in addition are counted as delivery time window violations,
	and for pickup-and-delivery instances also as pickup time window violations. Items that are delivered twice, or (for pickup-and-delivery instances) that are picked up twice
	or delivered without having been picked up before, or picked up without being delivered afterwards, raise a \texttt{RuntimeError}.}

{\bf Example.} The following example shows the report generated for a solution with two tours, in which item \texttt{6} is delivered roughly 42 minutes too late by tour \texttt{1},
and there are two other items that are delivered late within their time windows, but not too late -- one less than 5 minutes before the end of the time window, and the other less than 10, but at least
5 minutes before the end of its time window:
\begin{verbatim}
{
   "aggregated": {
      "num_tours": 2,
      "working_time": 9923531,
      "driving_time": 8123531,
      "waiting_time": 0,
      "distance": 81693,
      "cost": 455.1307249519792,
      "time_window_violations": {
         "6": 2541119
      },
      "max_time_window_violation": 2541119,
      "slack_histogram": {
         "0": 1, "5": 1, "10": 1, "15": 0
      }
   },
   "tour_results": [
      {
         "tour_id": "1",
         "working_time": 6020769,
         "driving_time": 5120769,
         "waiting_time": 0,
         "distance": 54199,
         "time_window_violations": {
            "6": 2541119
         },
         "max_time_window_violation": 2541119,
         "slack_histogram": {
            "0": 1, "5": 0, "10": 0, "15": 0
         }
      },
      {
         "tour_id": "2",
         "working_time": 3902762,
         "driving_time": 3002762,
         "waiting_time": 0,
         "distance": 27494,
         "time_window_violations": {},
         "max_time_window_violation": 0,
         "slack_histogram": {
            "0": 0, "5": 1, "10": 1, "15": 0
         }
      }
   ]
}
\end{verbatim}

\section{Solutions}
For each \REV{delivery-only} benchmark instance, we provide the solution reported in the high effort column of Table 2 in \cite{vrptdt2022} in the file \texttt{city\_size\_solution.json} (e.g.,
\texttt{berlin\_2000\_solution.json} for the \texttt{berlin\_2000} instance).
\REV{For each pickup-and-delivery benchmark instance, we provide the solution reported in the high effort column of Table 3 in \cite{vrptdt2022} in the file \linebreak \texttt{city\_size\_pdp\_solution.json} (e.g.,
	\texttt{berlin\_2000\_pdp\_solution.json} for the \texttt{berlin\_2000\_pdp} instance).
	Figure \ref{fig:tours} shows the tours in all of these solutions for the instances of size 1000.
}

\begin{figure}[htb!]
	\captionsetup[subfigure]{aboveskip=-3mm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/berlin.pdf}
		\end{center}
		\caption{Berlin (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/berlin_pdp.pdf}
		\end{center}
		\caption{Berlin (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/cincinnati.pdf}
		\end{center}
		\caption{Cincinnati (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/cincinnati_pdp.pdf}
		\end{center}
		\caption{Cincinnati (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/kyiv.pdf}
		\end{center}
		\caption{Kyiv (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/kyiv_pdp.pdf}
		\end{center}
		\caption{Kyiv (pickup-and-delivery)}
	\end{subfigure}

	\bigskip
	\caption{\REV{Tours in the solutions reported in the high effort column of Tables 2 and 3 in \cite{vrptdt2022} for each of the instances of size 1000.
			The depot is shown by a star, and pickup and delivery locations by dots.}}
	\label{fig:tours}
\end{figure}

\begin{figure}[htb!]
	\ContinuedFloat
	\captionsetup[subfigure]{aboveskip=-3mm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/london.pdf}
		\end{center}
		\caption{London (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/london_pdp.pdf}
		\end{center}
		\caption{London (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/madrid.pdf}
		\end{center}
		\caption{Madrid (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/madrid_pdp.pdf}
		\end{center}
		\caption{Madrid (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/nairobi.pdf}
		\end{center}
		\caption{Nairobi (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/nairobi_pdp.pdf}
		\end{center}
		\caption{Nairobi (pickup-and-delivery)}
	\end{subfigure}

	\bigskip
	\caption{\REV{Tours in the solutions reported in the high effort column of Tables 2 and 3 in \cite{vrptdt2022} for each of the instances of size 1000.
			The depot is shown by a star, and pickup and delivery locations by dots.}}
\end{figure}

\begin{figure}[htb!]
	\ContinuedFloat
	\captionsetup[subfigure]{aboveskip=-3mm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/new_york.pdf}
		\end{center}
		\caption{New York (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/new_york_pdp.pdf}
		\end{center}
		\caption{New York (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/san_francisco.pdf}
		\end{center}
		\caption{San Francisco (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=5cm]{images/san_francisco_pdp.pdf}
		\end{center}
		\caption{San Francisco (pickup-and-delivery)}
	\end{subfigure}

	\vspace{0.75cm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=4.75cm]{images/sao_paulo.pdf}
		\end{center}
		\caption{S\~{a}o Paulo (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=4.75cm]{images/sao_paulo_pdp.pdf}
		\end{center}
		\caption{S\~{a}o Paulo (pickup-and-delivery)}
	\end{subfigure}

	\bigskip
	\caption{\REV{Tours in the solutions reported in the high effort column of Tables 2 and 3 in \cite{vrptdt2022} for each of the instances of size 1000.
			The depot is shown by a star, and pickup and delivery locations by dots.}}
\end{figure}

\begin{figure}[htb!]
	\ContinuedFloat
	\captionsetup[subfigure]{aboveskip=-3mm}
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=6cm]{images/seattle.pdf}
		\end{center}
		\caption{Seattle (delivery-only)}
	\end{subfigure}
	\hfill %%
	\begin{subfigure}[b]{0.49\textwidth}
		\begin{center}
			\includegraphics[height=6cm]{images/seattle_pdp.pdf}
		\end{center}
		\caption{Seattle (pickup-and-delivery)}
	\end{subfigure}

	\bigskip
	\caption{\REV{Tours in the solutions reported in the high effort column of Tables 2 and 3 in \cite{vrptdt2022} for each of the instances of size 1000.
			The depot is shown by a star, and pickup and delivery locations by dots.}}
\end{figure}

\bibliographystyle{acm}
\bibliography{vrp_tdt_benchmark_references.bib}

\end{document}


